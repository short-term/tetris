#include "start.h"
#include "ui_start.h"

#include <QVBoxLayout>

start::start(QWidget *parent) :
    QWidget(parent),
    ui(new Ui::start)
{

    ui->setupUi(this);
    page = new Page();
    oper = new Operation();

    this->setWindowTitle("game start");

    QPalette palette;

    palette.setBrush(QPalette::Background, QBrush(QPixmap(":/img/background.png")));
    this->setPalette(palette);

    start_btn = new QPushButton("开始游戏", this);
    start_btn->setStyleSheet("QPushButton{font-family:'华文琥珀';"
                                        "font-size:23px;"
                                        "color:#595959;"
                                        "background-color:#E39652;"
                                        "padding:8px;"
                                        "border:3px solid #D3B719;"
                                        "border-style:ridge;}"


                             "QPushButton:hover{background-color:#EDDA67}"

                             "QPushButton:pressed{background-color:#F2EC60}");

    data_btn = new QPushButton("历史记录", this);
    data_btn->setStyleSheet("QPushButton{font-family:'华文琥珀';"
                                        "font-size:23px;"
                                        "color:#595959;"
                                        "background-color:#E39652;"
                                        "padding:8px;"
                                        "border:3px solid #D3B719;"
                                        "border-style:ridge;}"


                             "QPushButton:hover{background-color:#EDDA67}"

                             "QPushButton:pressed{background-color:#F2EC60}");

    config_btn = new QPushButton("设置", this);
    config_btn->setStyleSheet("QPushButton{font-family:'华文琥珀';"
                                        "font-size:23px;"
                                        "color:#595959;"
                                        "background-color:#E39652;"
                                        "padding:8px;"
                                        "border:3px solid #D3B719;"
                                        "border-style:ridge;}"


                             "QPushButton:hover{background-color:#EDDA67}"

                             "QPushButton:pressed{background-color:#F2EC60}");

    quit_btn = new QPushButton("退出游戏", this);
    quit_btn->setStyleSheet("QPushButton{font-family:'华文琥珀';"
                                        "font-size:23px;"
                                        "color:#595959;"
                                        "background-color:#E39652;"
                                        "padding:8px;"
                                        "border:3px solid #D3B719;"
                                        "border-style:ridge;}"


                             "QPushButton:hover{background-color:#EDDA67}"

                             "QPushButton:pressed{background-color:#F2EC60}");


    QVBoxLayout *layout = new QVBoxLayout(this);
    layout->setSizeConstraint(QLayout::SetFixedSize);

    layout->addWidget(start_btn);
    layout->addWidget(data_btn);
    layout->addWidget(config_btn);
    layout->addWidget(quit_btn);

    layout->setMargin(700);
    layout->setContentsMargins(720,370,160,80);

    this->setLayout(layout);


    connect(start_btn, &QPushButton::clicked, this, &start::gameStart);
    connect(data_btn, &QPushButton::clicked,  this, &start::checkData);
    connect(config_btn, &QPushButton::clicked, this, &start::configSet);
    connect(quit_btn, &QPushButton::clicked, this, &start::quitGame);

}

void start::gameStart(){
    this->hide();
        emit showGame(page, oper); //窗口跳转，跳到游戏界面
}


void start::checkData(){ //也是窗口跳转, 跳转到历史记录页面，可返回
    this->hide();
        emit showData(page);
}

void start::configSet(){ //跳转到设置界面，可查看键位设置以及修改,可返回
    this->hide();
        emit setConfig(oper);
}


void start::quitGame(){ //退出窗口
    this->close();
}

void start::returnStart(){ //返回到当前窗口
    this->show();
}


start::~start()
{
    delete ui;

    delete start_btn;
    delete data_btn;
    delete config_btn;
    delete quit_btn;

    delete page;
    delete oper;
}
