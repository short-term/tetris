#include "mainwindow.h"
#include "start.h"
#include "record.h"
#include "config.h"

#include <QApplication>

int main(int argc, char *argv[])
{
    QApplication a(argc, argv);

    start s;
    s.show();


    MainWindow w;
    QObject::connect(&s, SIGNAL(showGame(Page *, Operation *)), &w, SLOT(uiGameShow(Page *, Operation *)));
    QObject::connect(&w, SIGNAL(returnStart()), &s, SLOT(returnStart()));

    record r;
    QObject::connect(&s, SIGNAL(showData(Page *)), &r, SLOT(recordShow(Page *)));
    QObject::connect(&r, SIGNAL(backStart()), &s, SLOT(returnStart()));

    config c;
    QObject::connect(&s, SIGNAL(setConfig(Operation *)), &c, SLOT(showConfig(Operation *)));
    QObject::connect(&c, SIGNAL(backStart()), &s, SLOT(returnStart()));

    return a.exec();
}
