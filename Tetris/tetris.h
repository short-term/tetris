#ifndef TETRIS_H
#define TETRIS_H

#define MAXX 10
#define MAXY 20 //游戏窗口的格数
#define NEXX 6
#define NEXY 6  //下一个窗口的格数

#define WIDTH 30    //单格的宽度
#define HEIGHT 30   //单格的高度
#define INTERVAL 4  //单格之间的间隔
#define COUNT 4
#define LEN 30 //每个block的边长
#define INTERNAL 4// block之间空隙大小

enum Operation_type{rotate,moveLeft,moveRight};//判断操作类型


class Block{
public:
    friend class Item;
    Block(int x = -1, int y = -1):gx(x),gy(y){}

    void setX(int x){
        gx = x;
    }

    void setY(int y){
        gy = y;
    }

    int getX(){
        return gx;
    }

    int getY(){
        return gy;
    }

private:
    int gx;
    int gy; //格点坐标，从0开始
};


class Item{
public:
    Item(){
        type = -1;
        color = 0;
    }

    Item(int type_, Block b_[], int color_): type(type_), color(color_){
        for(int i = 0; i < 4; i++){
            b[i] = b_[i];
        }
        start_ = b[0].getY();
    }

    void moveLeft(int (*blocks)[10]);
    void moveRight(int (*blocks)[10]);
    void getDown(int (*blocks)[10]);
    void rotate(int (*blocks)[10]);
    int getType(){return type;}
    int getX_(int i){return b[i].getX();}
    int getY_(int i){return b[i].getY();}
    int getStart(){return  start_;}
    int getColor() {return color;}

    bool is_legal(int op_type); //判断操作是否合法，合法返回true，不合法返回false

    static Item newItem(); //随机生成一个新的Item

    Block b[4]; //每个Item由4个block组成
private:
    int type;  //有七种type的方格
    int start_;
    int color; // 有5种颜色

};

class Tetris{
public:
    Tetris(){
        cube = Item::newItem();
        nextCube = Item::newItem();
        score = 0;
        for(int i = 0; i < 20; i++)
            for(int j = 0 ; j < 10; j++)
                blocks[i][j] = 0;
    }

    void moveDown(){cube.getDown(blocks);}
    bool moveLeft();
    bool moveRight();
    //bool rotateLeft();
    bool rotate(); //判断是不是可以移动,可以返回true，否则返回flase


    bool isOver(); //判断游戏是否结束(是否到顶了
    bool haveFullLine(); //判断是否有整行
    bool isBottom(); //判断当前cube能否继续往下移动
    bool is_legal(int op_type); //判断操作是否合法，合法返回true，不合法返回false


    void elimination(); //消掉有整行的块
    void updateItem();
    void updateNextItem();
    //void updatelock(); //更新blocks的状态 (在移动item时同步更新)
    void clear(); //清空屏幕上的block

    static int getWidth(){
        return MAXX*(LEN+INTERNAL)-INTERNAL;
    }
    static int getHeight(){
        return MAXY*(LEN+INTERNAL)-INTERNAL;
    }
    static int getNextWidth(){
        return NEXX*(LEN+INTERNAL)-INTERNAL;
    }
    static int getNextHeight(){
        return NEXY*(LEN+INTERNAL)-INTERNAL;
    }

    Item getCube(){return cube;}
    Item getNextCube(){return nextCube;}
    int getBlocks(int idx, int idy){return blocks[idx][idy];}
    int *getBlocks(int idx){return blocks[idx];}
    int getScore(){return score;}

private:
    Item cube;
    Item nextCube;
    int blocks[20][10];
    int score;
};

#endif // TETRIS_H
