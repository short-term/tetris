#include "record.h"
#include "ui_record.h"

record::record(QWidget *parent) :
    QWidget(parent),
    ui(new Ui::record)
{
    ui->setupUi(this);
    this->setWindowTitle("records");

    table = new QTableWidget(this);
    clear = new QPushButton("清空", this);
    clear->setStyleSheet("QPushButton{font-family:'华文琥珀';"
                             "font-size:23px;"
                             "color:#595959;"
                             "background-color:#ECAF19;"
                             "padding:8px;"
                             "border:3px solid #D3B719;"
                             "border-style:ridge;}"


                             "QPushButton:hover{background-color:#EDDA67}"

                             "QPushButton:pressed{background-color:#F2EC60}");
    back = new QPushButton("返回", this);
    back->setStyleSheet("QPushButton{font-family:'华文琥珀';"
                             "font-size:23px;"
                             "color:#595959;"
                             "background-color:#ECAF19;"
                             "padding:8px;"
                             "border:3px solid #D3B719;"
                             "border-style:ridge;}"


                             "QPushButton:hover{background-color:#EDDA67}"

                             "QPushButton:pressed{background-color:#F2EC60}");

    table->setColumnCount(3);
    table->verticalHeader()->setVisible(false);

    QStringList headers = {"序号", "游戏时间", "得分"};

    table->setHorizontalHeaderLabels(headers);
    table->horizontalHeader()->setStyleSheet("QHeaderView::section {"
                                             "font-family:'幼圆';"
                                             "font-weight:bold;"
                                             "border:3px groove #D3B719;"
                                             "background-color:#EDE180}");

    //table->verticalHeader()->setHidden(true);
    //table->horizontalHeader()->setHidden(true);

    table->setEditTriggers(QAbstractItemView::NoEditTriggers);

    table->setStyleSheet("QTableWidget{font-family:'Berlin Sans FB';"
                                    "color:#595959;"
                                    "background-color:#F5EEB8;"
                                    "border:2px solid #D3B719;}"
                         "QHeaderView{background-color:#EDE180}"
                         "QTableWidget::item::selected{background-color:#EEC1A2}"
                         "QScrollBar{background-color:#F6C041}"
                         "QScrollBar:horizontal{background-color:#F9D37A}");

    table->setColumnWidth(0, 190);
    table->setColumnWidth(1, 190);
    table->setColumnWidth(2, 190);

    QGridLayout *layout = new QGridLayout(this);

    layout->addWidget(table, 0, 0, 1, 2);
    layout->addWidget(clear, 1, 0);
    layout->addWidget(back, 1, 1);

    this->setLayout(layout);

    QPalette palette;

    palette.setBrush(QPalette::Background, QBrush(QPixmap(":/img/background3.png")));
    this->setPalette(palette);

    this->setMinimumSize(600,400);

    connect(clear, &QPushButton::clicked, this, &record::clearRecord);
    connect(back, &QPushButton::clicked, this, &record::onClickedBack);
    connect(this, &record::dataChange, this, &record::onHanddleChange);
}

void record::clearRecord(){
    page->clearData();
        emit dataChange();
}

void record::onClickedBack(){
    this->hide();
        emit backStart();
}

void record::onHanddleChange(){
    if(page->getNum() == 0){
        table->clearContents();
        table->setRowCount(0);
        return ;
    }
  //  qDebug() << page->getNum() << " " << table->rowCount() << endl;

    if(page->getNum() > table->rowCount()){
        for(int i = table->rowCount(); i < page->getNum(); i++){
            int row = table->rowCount();
            Data data = page->getData(i);
            table->insertRow(row);
            table->setItem(row, 0, new QTableWidgetItem(QString::number(data.getID())));
            table->setItem(row, 1, new QTableWidgetItem(data.getTime()));
            table->setItem(row, 2, new QTableWidgetItem(QString::number(data.getScore())));
        }
    }
}

void record::recordShow(Page *page_){
    this->show();
    page = page_;
        emit dataChange();
}

record::~record()
{
    delete ui;

    delete clear;
    delete back;

    delete table;
}
