#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include "tetris.h"
#include "tetrisbox.h"
#include "nexttetris.h"
#include "data.h"
#include "operation.h"

#include <QMainWindow>
#include <QDateTime>
#include <QGridLayout>
#include <QPushButton>
#include <QLabel>
#include <QTimer>
#include <QMessageBox>

enum Status{
    ON = 0,OFF,PAUSE
};

QT_BEGIN_NAMESPACE
namespace Ui { class MainWindow; }
QT_END_NAMESPACE

class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    MainWindow(QWidget *parent = nullptr);
    void keyPressEvent(QKeyEvent *event);
    void changeEvent(QEvent *event);

    ~MainWindow();

signals:
    void returnStart();


private:
    Ui::MainWindow *ui;
    Tetris tetris;
    QTimer *timer;
    Tetrisbox *terisbox;
    Nexttetris *nexttetris;
    QGridLayout *mainlayout;

    Page *page;
    Operation *oper;

    QPushButton *gameStart;
    QPushButton *gamePause;
    QPushButton *backStart;

    QLabel *nextLabel;
    QLabel *scoreTitle;
    QLabel *scoreLabel;

    Status sta;

private slots:
    void uiGameShow(Page *, Operation *);
    void onTimer();
    void onGameStart();
    void onGmaePause();
    void onBackStart();
};
#endif // MAINWINDOW_H
