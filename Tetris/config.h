#ifndef CONFIG_H
#define CONFIG_H

#include "operation.h"

#include <QWidget>
#include <QPushButton>
#include <QHBoxLayout>
#include <QGridLayout>
#include <QLabel>
#include <QLineEdit>

namespace Ui {
class config;
}

class config : public QWidget
{
    Q_OBJECT

public:
    explicit config(QWidget *parent = nullptr);
    ~config();

signals:
    void backStart();


private slots:
    void showConfig(Operation *oper);
    void onClickedBack();
    void resetConfig();

    void onMoveLeft();
    void onMoveRight();
    void onRotateLeft();
    void onRotateRight();
    void onGetDown();

private:
    Ui::config *ui;

    QPushButton *back;
    QPushButton *reset;

    QLabel *mleft;
    QLineEdit *mleft_in;

    QLabel *mright;
    QLineEdit *mright_in;

    QLabel *rleft;
    QLineEdit *rleft_in;

    QLabel *rright;
    QLineEdit *rright_in;

    QLabel *down;
    QLineEdit *down_in;

    Operation *oper;
};

#endif // CONFIG_H
