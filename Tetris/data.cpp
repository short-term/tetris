#include "data.h"
void Page::addData(int score, QString time){
    int new_id = num + 1;
    Data new_data(new_id,score,time);
    data.push_back(new_data);
    num++;
}

void Page::clearData(){
    data.clear();
    num = 0;
}
