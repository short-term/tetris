#include "mainwindow.h"
#include "ui_mainwindow.h"

MainWindow::MainWindow(QWidget *parent)
    : QMainWindow(parent)
    , ui(new Ui::MainWindow)
{
    ui->setupUi(this);

    terisbox = new Tetrisbox;
    //terisbox->setStyleSheet("border:3px solid #D3B719;");
    nexttetris = new Nexttetris;

    nextLabel = new QLabel("下一个Tetris:");
    nextLabel->setStyleSheet("QLabel{font-family:'幼圆';"
                         "font-size:23px;"
                         "color:#000000;"
                         "font-weight:bold;}");

    scoreTitle = new QLabel("当前得分:");
    scoreTitle->setStyleSheet("QLabel{font-family:'幼圆';"
                             "font-size:23px;"
                             "color:#595959;"
                             "font-weight:bold;}");

    scoreLabel = new QLabel("0");
    scoreLabel->setStyleSheet("QLabel{font-family:'Candara';"
                              "font-size:23px;"
                              "color:#595959;"
                              "font-weight:bold;"
                              "border:3px groove #D3B719;"
                              "background-color:#F9BB61;}");

    gameStart = new QPushButton("开始游戏");
    connect(gameStart, &QPushButton::clicked, this, &MainWindow::onGameStart);
    gameStart->setStyleSheet("QPushButton{font-family:'华文琥珀';"
                         "font-size:23px;"
                         "color:#595959;"
                         "background-color:#ECAF19;"
                         "padding:8px;"
                         "border:3px solid #D3B719;"
                         "border-style:ridge;}"


                         "QPushButton:hover{background-color:#EDDA67}"

                         "QPushButton:pressed{background-color:#F2EC60}");

    gamePause = new QPushButton("暂停游戏");
    gamePause->setShortcut(Qt::Key_Escape);
    connect(gamePause, &QPushButton::clicked, this, &MainWindow::onGmaePause);
    gamePause->setStyleSheet("QPushButton{font-family:'华文琥珀';"
                             "font-size:23px;"
                             "color:#595959;"
                             "background-color:#ECAF19;"
                             "padding:8px;"
                             "border:3px solid #D3B719;"
                             "border-style:ridge;}"


                             "QPushButton:hover{background-color:#EDDA67}"

                             "QPushButton:pressed{background-color:#F2EC60}");

    backStart = new QPushButton("返回");
    connect(backStart, &QPushButton::clicked, this, &MainWindow::onBackStart);
    backStart->setStyleSheet("QPushButton{font-family:'华文琥珀';"
                             "font-size:23px;"
                             "color:#595959;"
                             "background-color:#ECAF19;"
                             "padding:8px;"
                             "border:3px solid #D3B719;"
                             "border-style:ridge;}"


                             "QPushButton:hover{background-color:#EDDA67}"

                             "QPushButton:pressed{background-color:#F2EC60}");

    mainlayout = new QGridLayout;
    mainlayout->setHorizontalSpacing(20);
    mainlayout->setVerticalSpacing(20);
    mainlayout->setAlignment(Qt::AlignCenter);

    mainlayout->addWidget(terisbox, 0, 0, 14, 1);
    mainlayout->addWidget(nextLabel, 0, 1);
    mainlayout->addWidget(nexttetris, 1, 1, 1, 2);
    mainlayout->addWidget(gameStart, 3, 1, 2, 2);
    mainlayout->addWidget(gamePause, 5, 1, 2, 2);
    mainlayout->addWidget(backStart, 7, 1, 2, 2);
    mainlayout->addWidget(scoreTitle, 9, 1);
    mainlayout->addWidget(scoreLabel, 10, 1);

    QWidget *w = new QWidget(this);
    w->setLayout(mainlayout);
    setCentralWidget(w);

    //setPalette(Qt::gray);
    QPalette palette;
    palette.setBrush(QPalette::Background, QBrush(QPixmap(":/img/background2.jpg")));
    this->setPalette(palette);
    sta = OFF;
    setWindowTitle("Tetris");
    timer = new QTimer(this);
    connect(timer, &QTimer::timeout, this, &MainWindow::onTimer);

}

void MainWindow::uiGameShow(Page *page_, Operation *oper_){
    this->show();
    page = page_;
    oper = oper_;
}

void MainWindow::onTimer(){
    if(tetris.isBottom())
    {
        bool isok = false;
        if(tetris.isOver())
        {
            timer->stop();
            sta = Status::OFF;
            QDateTime current_date_time = QDateTime::currentDateTime();
            QString current_date =current_date_time.toString("yyyy.MM.dd");
            page->addData(tetris.getScore(),current_date);

            QMessageBox box;
            box.setText("你的得分是: " + QString::number(tetris.getScore()));
            box.setStyleSheet("QMessageBox{background-color:#E9DB69;"
                                          "border:3px solid #D3B719;}"
                              "QPushButton{font-family:'华文琥珀';"
                                "font-size:23px;"
                                "color:#595959;"
                                "background-color:#ECAF19;"
                                "padding:8px;"
                                "border:3px solid #D3B719;"
                                "border-style:ridge;}"
                              "QLabel{font-family:'幼圆';"
                                "font-size:23px;"
                                "color:#595959;"
                                "font-weight:bold;}");
          //  box.setFixedSize(1000,100);
            box.setWindowTitle("Game Over!");
            box.exec();
            isok = true;
        }
        if(!isok)
        {
            tetris.updateItem();
            tetris.updateNextItem();
            terisbox->updateTetris(tetris);
            nexttetris->updateTetris(tetris);
            scoreLabel->setNum(tetris.getScore());
        }
        if(isok)
        {
            tetris.clear();
            terisbox->updateTetris(tetris);
            nexttetris->updateTetris(tetris);
            scoreLabel->setNum(tetris.getScore());
        }
    }
    else {
        tetris.moveDown();
        terisbox->updateTetris(tetris);
        nexttetris->updateTetris(tetris);
    }
}

void MainWindow::onGameStart(){
    if(sta != Status::OFF) return ;
    tetris.clear();
    scoreLabel->setNum(0);
    terisbox->updateTetris(tetris);
    nexttetris->updateTetris(tetris);
    sta = Status::ON;
    timer->start(500);
}

void MainWindow::onGmaePause(){
    if(sta == Status::ON)
    {
        timer->stop();
        sta = Status::PAUSE;
    }
    else if (sta == Status::PAUSE) {
        timer->start();
        sta = Status::ON;
    }
}

void MainWindow::onBackStart(){
    if(sta != OFF) return;
    this->hide();
        emit returnStart();
}

void MainWindow::keyPressEvent(QKeyEvent *event) {
    if(sta == Status::ON) {
        if(event->key() == oper->getMoveLeft()) {
            if(tetris.moveLeft()) {
                terisbox->updateTetris(tetris);
            }
        }
        else if(event->key() == oper->getMoveRight()) {
            if(tetris.moveRight()) {
                terisbox->updateTetris(tetris);
            }
        }
        else if(event->key() == oper->getRotateLeft()) {
            if(tetris.rotate()) {
                terisbox->updateTetris(tetris);
            }
        }
        else if(event->key() == oper->getRotateRight()) {
            if(tetris.rotate()) {
                terisbox->updateTetris(tetris);
            }
        }
        else if(event->key() == oper->getDown()) {
            tetris.moveDown();
            terisbox->updateTetris(tetris);
        }
    }
}


void MainWindow::changeEvent(QEvent *event){
    if(event->type() != QEvent::WindowStateChange)
        return;
    if(windowState() == Qt::WindowMinimized)
        timer->stop();
}



MainWindow::~MainWindow()
{
    delete ui;

    delete timer;
    delete terisbox;
    delete nexttetris;
    delete scoreLabel;
    delete scoreTitle;
    delete gamePause;
    delete gameStart;
    delete backStart;
    delete mainlayout;
}

