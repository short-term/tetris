#include "tetrisbox.h"

Tetrisbox::Tetrisbox(QWidget *parent) : QWidget(parent) {
    //初始化item 和 block
    for(int i = 0; i < 20; i++)
    {
        for(int j = 0; j < 10; j++)
            blocks[i][j] = 0;
    }
    for (int i = 0; i < COUNT; i++) {
        cube.b[i].setX(-1);
        cube.b[i].setY(-1);
    }
    /*to be completed*/
    int w = Tetris::getWidth();
    int h = Tetris::getHeight();
    setFixedSize(w, h);

    QPalette palette;
    palette.setBrush(QPalette::Background, QBrush(QPixmap(":/img/background3.png")));
    this->setPalette(palette);
    //setPalette(QPalette(Qt::black));
    setWindowTitle("tetrisbox");
    setAutoFillBackground(true);
}

void Tetrisbox::updateTetris(Tetris tetris) {
    cube = tetris.getCube();

    //update blocks
    for(int i = 0; i < 20; i++)
    {
        for(int j = 0; j < 10; j++)
            blocks[i][j] = tetris.getBlocks(i,j);
    }

    repaint();
}

void Tetrisbox::paintEvent(QPaintEvent *event) {
    QPainter painter(this);

    for (int i = 0; i < MAXX; i++) {
        for (int j = 0; j < MAXY; j++) {
            int x = i * WIDTH + i * INTERVAL;
            int y = j * HEIGHT + j * INTERVAL;
//                painter.drawPixmap(x, y, WIDTH, HEIGHT, pix);


            switch(blocks[j][i]) {
            case 1: painter.drawPixmap(x, y, WIDTH, HEIGHT, QPixmap(":/img/blue.jpg")); break;
            case 2: painter.drawPixmap(x, y, WIDTH, HEIGHT, QPixmap(":/img/red.jpg")); break;
            case 3: painter.drawPixmap(x, y, WIDTH, HEIGHT, QPixmap(":/img/yellow.png")); break;
            case 4: painter.drawPixmap(x, y, WIDTH, HEIGHT, QPixmap(":/img/green.jpg")); break;
            case 5: painter.drawPixmap(x, y, WIDTH, HEIGHT, QPixmap(":/img/grey.jpg")); break;
            default: break;
            }

        }
    }




}
