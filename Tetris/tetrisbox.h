#ifndef TETRISBOX_H
#define TETRISBOX_H

#include "tetris.h"

#include <QWidget>
#include <QPen>
#include <QBrush>
#include <QColor>
#include <QPaintEvent>
#include <QPainter>
#include <QPalette>

#include <QLabel>
#include <QPixmap>

#include <QDebug>



class Tetrisbox: public QWidget
{
    Q_OBJECT
public:
    Tetrisbox(QWidget *parent = nullptr);
    void updateTetris(Tetris tetris);
    void paintEvent(QPaintEvent *event);

private:
    Item cube;
    int blocks[20][10];

};

#endif // TETRISBOX_H
