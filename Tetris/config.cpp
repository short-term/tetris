#include "config.h"
#include "ui_config.h"

config::config(QWidget *parent) :
    QWidget(parent),
    ui(new Ui::config)
{
    ui->setupUi(this);
    this->setWindowTitle("setting");

    reset = new QPushButton("重置", this);
    reset->setFixedSize(200, 50);
    reset->setStyleSheet("QPushButton{font-family:'华文琥珀';"
                             "font-size:23px;"
                             "color:#595959;"
                             "background-color:#ECAF19;"
                             "padding:8px;"
                             "border:3px solid #D3B719;"
                             "border-style:ridge;}"


                             "QPushButton:hover{background-color:#EDDA67}"

                             "QPushButton:pressed{background-color:#F2EC60}");

    back = new QPushButton("返回", this);
    back->setFixedSize(200, 50);
    back->setStyleSheet("QPushButton{font-family:'华文琥珀';"
                             "font-size:23px;"
                             "color:#595959;"
                             "background-color:#ECAF19;"
                             "padding:8px;"
                             "border:3px solid #D3B719;"
                             "border-style:ridge;}"


                             "QPushButton:hover{background-color:#EDDA67}"

                             "QPushButton:pressed{background-color:#F2EC60}");

    mleft = new QLabel("向左移动:", this);
    mleft->setStyleSheet("QLabel{font-family:'幼圆';"
                         "font-size:23px;"
                         "color:#595959;"
                         "font-weight:bold;}");
    mright = new QLabel("向右移动:", this);
    mright->setStyleSheet("QLabel{font-family:'幼圆';"
                         "font-size:23px;"
                         "color:#595959;"
                         "font-weight:bold;}");
    rleft = new QLabel("向左旋转:", this);
    rleft->setStyleSheet("QLabel{font-family:'幼圆';"
                         "font-size:23px;"
                         "color:#595959;"
                         "font-weight:bold;}");
    rright = new QLabel("向右旋转:", this);
    rright->setStyleSheet("QLabel{font-family:'幼圆';"
                         "font-size:23px;"
                         "color:#595959;"
                         "font-weight:bold;}");
    down = new QLabel("落下:", this);
    down->setStyleSheet("QLabel{font-family:'幼圆';"
                         "font-size:23px;"
                         "color:#595959;"
                         "font-weight:bold}");

    mleft_in = new QLineEdit("A", this);
    mleft_in->setMaxLength(1);
    mleft_in->setStyleSheet("QLineEdit{font-family:'Berlin Sans FB';"
                            "color:#595959;"
                            "border:3px groove #D3B719;"
                            "background-color:#F2C75E}"
                            "QLineEdit:hover{background-color:#EDDA67}");

    mright_in = new QLineEdit("D", this);
    mright_in->setMaxLength(1);
    mright_in->setStyleSheet("QLineEdit{font-family:'Berlin Sans FB';"
                            "color:#595959;"
                            "border:3px groove #D3B719;"
                            "background-color:#F2C75E}"
                             "QLineEdit:hover{background-color:#EDDA67}");

    rleft_in = new QLineEdit("J", this);
    rleft_in->setMaxLength(1);
    rleft_in->setStyleSheet("QLineEdit{font-family:'Berlin Sans FB';"
                            "color:#595959;"
                            "border:3px groove #D3B719;"
                            "background-color:#F2C75E}"
                            "QLineEdit:hover{background-color:#EDDA67}");

    rright_in = new QLineEdit("L", this);
    rright_in->setMaxLength(1);
    rright_in->setStyleSheet("QLineEdit{font-family:'Berlin Sans FB';"
                            "color:#595959;"
                            "border:3px groove #D3B719;"
                            "background-color:#F2C75E}"
                             "QLineEdit:hover{background-color:#EDDA67}");

    down_in = new QLineEdit("S", this);
    down_in->setMaxLength(1);
    down_in->setStyleSheet("QLineEdit{font-family:'Berlin Sans FB';"
                           "color:#595959;"
                            "border:3px groove #D3B719;"
                            "background-color:#F2C75E}"
                           "QLineEdit:hover{background-color:#EDDA67}");

    //this->setStyleSheet("border-image:url(:/img/background.png)");
    QPalette palette;

    palette.setBrush(QPalette::Background, QBrush(QPixmap(":/img/background3.png")));
    this->setPalette(palette);

    QGridLayout *layout = new QGridLayout(this);

    layout->addWidget(mleft, 0, 0);
    layout->addWidget(mleft_in, 0, 1);

    layout->addWidget(mright, 1, 0);
    layout->addWidget(mright_in, 1, 1);

    layout->addWidget(rleft, 2, 0);
    layout->addWidget(rleft_in, 2, 1);

    layout->addWidget(rright, 3, 0);
    layout->addWidget(rright_in, 3, 1);

    layout->addWidget(down, 4, 0);
    layout->addWidget(down_in, 4, 1);

    layout->addWidget(reset, 5, 0);
    layout->addWidget(back, 5, 1);

    this->setLayout(layout);

    connect(reset, &QPushButton::clicked, this, &config::resetConfig);
    connect(back, &QPushButton::clicked, this, &config::onClickedBack);

    connect(mleft_in, &QLineEdit::textChanged, this, &config::onMoveLeft);
    connect(mright_in, &QLineEdit::textChanged, this, &config::onMoveRight);
    connect(rleft_in, &QLineEdit::textChanged, this, &config::onRotateLeft);
    connect(rright_in, &QLineEdit::textChanged, this, &config::onRotateRight);
    connect(down_in, &QLineEdit::textChanged, this, &config::onGetDown);
}

void config::resetConfig(){
    oper->setDefault();
    mleft_in->setText("A");
    mright_in->setText("D");
    rleft_in->setText("J");
    rright_in->setText("L");
    down_in->setText("S");
}

void config::onClickedBack(){
    this->hide();
        emit backStart();
}

void config::onMoveLeft(){
    QString keyString = mleft_in->text();
    if(keyString[0] < 'A' || keyString[0] > 'Z'){
        mleft_in->undo();
        return ;
    }
    oper->setMoveLeft(keyString[0]);
}

void config::onMoveRight(){
    QString keyString = mright_in->text();
    if(keyString[0] < 'A' || keyString[0] > 'Z'){
        mright_in->undo();
        return ;
    }
    oper->setMoveRight(keyString[0]);
}

void config::onRotateLeft(){
    QString keyString = rleft_in->text();
    if(keyString[0] < 'A' || keyString[0] > 'Z'){
        rleft_in->undo();
        return ;
    }
    oper->setRotateLeft(keyString[0]);
}

void config::onRotateRight(){
    QString keyString = rright_in->text();
    if(keyString[0] < 'A' || keyString[0] > 'Z'){
        rright_in->undo();
        return ;
    }
    oper->setMoveRight(keyString[0]);
}

void config::onGetDown(){
    QString keyString = down_in->text();
    if(keyString[0] < 'A' || keyString[0] > 'Z'){
        down_in->undo();
        return ;
    }
    oper->setGetDown(keyString[0]);
}

void config::showConfig(Operation *oper_){
    this->show();
    oper = oper_;
}


config::~config()
{
    delete ui;

    delete mleft;
    delete mleft_in;
    delete mright;
    delete mright_in;
    delete rleft;
    delete rleft_in;
    delete rright;
    delete rright_in;
    delete down;
    delete down_in;

    delete reset;
    delete back;
}
