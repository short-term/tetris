#ifndef START_H
#define START_H

#include "data.h"
#include "operation.h"

#include <QWidget>
#include <QPushButton>


namespace Ui {
class start;
}

class start : public QWidget
{
    Q_OBJECT

public:
    explicit start(QWidget *parent = nullptr);
    ~start();

signals:
    void showGame(Page *, Operation *);
    void showData(Page *);
    void setConfig(Operation *);


private:
    Ui::start *ui;
    QPushButton *start_btn;
    QPushButton *data_btn;
    QPushButton *config_btn;
    QPushButton *quit_btn;

    Page *page;
    Operation *oper;

private slots:
    void gameStart();
    void checkData();
    void configSet();
    void quitGame();

    void returnStart();

};


#endif // START_H
