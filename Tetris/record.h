#ifndef RECORD_H
#define RECORD_H

#include "data.h"

#include <QWidget>
#include <QHeaderView>
#include <QPushButton>
#include <QHBoxLayout>
#include <QTableWidget>
#include <QGridLayout>
#include <QHeaderView>
#include <QDebug>
#include <QHeaderView>

namespace Ui {
class record;
}

class record : public QWidget
{
    Q_OBJECT

public:
    explicit record(QWidget *parent = nullptr);
    ~record();

signals:
   void backStart();
   void dataChange();

private slots:
   void clearRecord();
   void onClickedBack();

   void recordShow(Page *);
   void onHanddleChange();

private:
    Ui::record *ui;

    QPushButton *back;
    QPushButton *clear;

    QTableWidget *table;

    Page *page;
};

#endif // RECORD_H
