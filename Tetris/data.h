#ifndef DATA_H
#define DATA_H

#include <QString>
#include <vector>
#include <fstream>
#include <iostream>
#include <algorithm>

class Data{ //id是按顺序加上的，不保存到文件中
public:
    Data(int id_, int score_, QString time_):id(id_),score(score_),time(time_){}

    int getID(){
        return id;
    }

    int getScore(){
        return score;
    }

    QString getTime(){
        return time;
    }

private:
    int id;
    int score;
    QString time;
};



class Page{
public:
    Page(){ //构造时从文件中读取数据到data，默认按时间从低到高排序
        file.open("record.txt");
        num = 0;
        if(file.is_open())
        {
            while(!file.eof())
            {
                num++;
                int score_;
                std::string str;
                QString time_;
                file >> score_ >> str;
                time_ = QString::fromStdString(str);
                Data new_data(num,score_,time_);
                if(time_.size() > 1)
                    data.push_back(new_data);
            }
            num = data.size();
            file.close();
        }
        else
        {
            std::ofstream outfile("record.txt");
            outfile.close();
        }
    }

    void addData(int score, QString time);
    void clearData(); //清空所有数据

    Data getData(int idx){return data[idx];}
    int getNum(){return num;}

    ~Page(){ //析构时先写入data到文件,然后再释放
        std::ofstream ofs;
        ofs.open("record.txt",std::ios::ate);
        if(ofs.is_open())
        {
            for(std::vector<Data>::iterator it = data.begin(); it != data.end(); it++)
            {
                int score = it->getScore();
                QString time = it->getTime();
                std::string str = time.toStdString();
                ofs << score << " " << str << std::endl;
            }
            ofs.close();
            data.clear();
        }
    }



private:
    std::vector<Data> data;
    int num; //数据量
    std::fstream file;
};


#endif // DATA_H
