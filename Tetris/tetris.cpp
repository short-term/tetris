#include "tetris.h"
#include <ctime>
#include <random>
/*
    这一部分实现类Item中的功能函数
*/

Item Item::newItem()
{
    std::default_random_engine e(time(nullptr));
    std::uniform_int_distribution<int> u1(0,6);
    std::uniform_int_distribution<int> u2(2,6);
    srand(time(nullptr));

    int item_type;
    item_type = rand() % 7;
    int item_color;
    item_color = rand() % 5 + 1; // color 从1到5
    Block B0, B1, B2, B3;
    int start_location = u2(e);
    B0.setX(0);
    B0.setY(start_location);
    Block b[4];
    switch (item_type)
    {
    case 0:
    {
        B1.setY(B0.getY() + 1);
        B1.setX(0);
        B2.setY(B0.getY());
        B2.setX(1);
        B3.setY(B0.getY() + 1);
        B3.setX(1);
        break;
    }
    case 1:
    {
        B1.setY(B0.getY() + 1);
        B2.setY(B0.getY() + 2);
        B3.setY(B0.getY() + 3);
        B1.setX(0);
        B2.setX(0);
        B3.setX(0);
        break;
    }
    case 2:
    {
        B1.setY(B0.getY());
        B1.setX(1);
        B2.setY(B0.getY() + 1);
        B2.setX(1);
        B3.setY(B0.getY() + 2);
        B3.setX(1);
        break;
    }
    case 3:
    {
        B1.setY(B0.getY() - 2);
        B1.setX(1);
        B2.setY(B0.getY() - 1);
        B2.setX(1);
        B3.setY(B0.getY());
        B3.setX(1);
        break;
    }
    case 4:
    {
        B1.setY(B0.getY() + 1);
        B1.setX(0);
        B2.setY(B0.getY() + 1);
        B2.setX(1);
        B3.setY(B0.getY() + 2);
        B3.setX(1);
        break;
    }
    case 5:
    {
        B1.setY(B0.getY() + 1);
        B1.setX(0);
        B2.setY(B0.getY() - 1);
        B2.setX(1);
        B3.setY(B0.getY());
        B3.setX(1);
        break;
    }
    case 6:
    {
        B1.setY(B0.getY() - 1);
        B1.setX(1);
        B2.setY(B0.getY());
        B2.setX(1);
        B3.setY(B0.getY() + 1);
        B3.setX(1);
        break;
    }
    default:
        break;
    }
    b[0] = B0;
    b[1] = B1;
    b[2] = B2;
    b[3] = B3;
    Item return_item(item_type, b, item_color);
    return return_item;
}

void Item::moveLeft(int (*blocks)[10])
{
    bool reachleft = false;
    for(int i = 0; i < COUNT; i++)
        blocks[b[i].getX()][b[i].getY()] = 0;
    for(int i = 0; i < COUNT; i++)
    {
        if(b[i].getY() - 1 < 0 || blocks[b[i].getX()][b[i].getY() - 1] >= 1)
            reachleft = true;
    }
    if(reachleft)
    {
        for(int i = 0; i < COUNT; i++)
            blocks[b[i].getX()][b[i].getY()] = color;
        return;
    }
    for (int i = 0; i < COUNT; i++)
    {
        b[i].setX(b[i].getX());
        b[i].setY(b[i].getY() - 1);
        blocks[b[i].getX()][b[i].getY()] = color;
    }
    return;
}

void Item::moveRight(int (*blocks)[10])
{
    bool reachright = false;
    for(int i = 0; i < COUNT; i++)
        blocks[b[i].getX()][b[i].getY()] = 0;
    for(int i = 0; i < COUNT; i++)
    {
        if(b[i].getY() + 1 > 9 || blocks[b[i].getX()][b[i].getY() + 1] >= 1)
            reachright = true;
    }
    if(reachright)
    {
        for(int i = 0; i < COUNT; i++)
            blocks[b[i].getX()][b[i].getY()] = color;
        return;
    }
    for(int i = 0; i < COUNT; i++)
        blocks[b[i].getX()][b[i].getY()] = 0;
    for (int i = 0; i < COUNT; i++)
    {
        b[i].setX(b[i].getX());
        b[i].setY(b[i].getY() + 1);
        blocks[b[i].getX()][b[i].getY()] = color;
    }
    return;
}

void Item::getDown(int (*blocks)[10])
{
    bool reachbottom = false;
    for(int i = 0; i < COUNT; i++)
        blocks[b[i].getX()][b[i].getY()] = 0;
    for(int i = 0; i < COUNT; i++)
    {
        if(b[i].getX() + 1 > 19 || blocks[b[i].getX() + 1][b[i].getY()] >= 1)
            reachbottom = true;
    }
    if(reachbottom)
    {
        for(int i = 0; i < COUNT; i++)
            blocks[b[i].getX()][b[i].getY()] = color;
        return;
    }
    for (int i = 0; i < COUNT; i++)
    {
        b[i].setX(b[i].getX() + 1);
        b[i].setY(b[i].getY());
        blocks[b[i].getX()][b[i].getY()] = color;
    }
    return;
}

void Item::rotate(int (*blocks)[10])
{
    for(int i = 0; i < COUNT; i++)
        blocks[b[i].getX()][b[i].getY()] = 0;
    switch (type)
    {
    case 0:
        break;
    case 1:
        if(b[0].getX() == b[3].getX())
        {
            b[0].setY(b[1].getY());
            b[2].setY(b[1].getY());
            b[3].setY(b[1].getY());
            b[0].setX(b[1].getX() - 1);
            b[2].setX(b[1].getX() + 1);
            b[3].setX(b[1].getX() + 2);
        }
        else {
            b[0].setY(b[1].getY() - 1);
            b[2].setY(b[1].getY() + 1);
            b[3].setY(b[1].getY() + 2);
            b[0].setX(b[1].getX());
            b[2].setX(b[1].getX());
            b[3].setX(b[1].getX());
        }
        break;
    case 2:
        if(b[0].getX() < b[1].getX() && b[0].getY() == b[1].getY())
        {
            b[0].setY(b[2].getY() + 1);
            b[1].setY(b[2].getY());
            b[3].setY(b[2].getY());
            b[0].setX(b[2].getX() - 1);
            b[1].setX(b[2].getX() - 1);
            b[3].setX(b[2].getX() + 1);
        }
        else if (b[0].getX() > b[1].getX() && b[0].getY() == b[1].getY()) {
            b[0].setY(b[2].getY() - 1);
            b[1].setY(b[2].getY());
            b[3].setY(b[2].getY());
            b[0].setX(b[2].getX() + 1);
            b[1].setX(b[2].getX() + 1);
            b[3].setX(b[2].getX() - 1);
        }
        else if (b[0].getX() == b[1].getX() && b[0].getY() > b[1].getY()) {
            b[0].setY(b[2].getY() + 1);
            b[1].setY(b[2].getY() + 1);
            b[3].setY(b[2].getY() - 1);
            b[0].setX(b[2].getX() + 1);
            b[1].setX(b[2].getX());
            b[3].setX(b[2].getX());
        }
        else if (b[0].getX() == b[1].getX() && b[0].getY() < b[1].getY()) {
            b[0].setY(b[2].getY() - 1);
            b[1].setY(b[2].getY() - 1);
            b[3].setY(b[2].getY() + 1);
            b[0].setX(b[2].getX() - 1);
            b[1].setX(b[2].getX());
            b[3].setX(b[2].getX());
        }
        break;
    case 3:
        if(b[0].getX() < b[3].getX() && b[0].getY() == b[3].getY())
        {
            b[0].setY(b[2].getY() + 1);
            b[1].setY(b[2].getY());
            b[3].setY(b[2].getY());
            b[0].setX(b[2].getX() + 1);
            b[1].setX(b[2].getX() - 1);
            b[3].setX(b[2].getX() + 1);
        }
        else if (b[0].getX() > b[3].getX() && b[0].getY() == b[3].getY()) {
            b[0].setY(b[2].getY() - 1);
            b[1].setY(b[2].getY());
            b[3].setY(b[2].getY());
            b[0].setX(b[2].getX() - 1);
            b[1].setX(b[2].getX() + 1);
            b[3].setX(b[2].getX() - 1);
        }
        else if (b[0].getX() == b[3].getX() && b[0].getY() > b[3].getY()) {
            b[0].setY(b[2].getY() - 1);
            b[1].setY(b[2].getY() + 1);
            b[3].setY(b[2].getY() - 1);
            b[0].setX(b[2].getX() + 1);
            b[1].setX(b[2].getX());
            b[3].setX(b[2].getX());
        }
        else if (b[0].getX() == b[3].getX() && b[0].getY() < b[3].getY()) {
            b[0].setY(b[2].getY() + 1);
            b[1].setY(b[2].getY() - 1);
            b[3].setY(b[2].getY() + 1);
            b[0].setX(b[2].getX() - 1);
            b[1].setX(b[2].getX());
            b[3].setX(b[2].getX());
        }
        break;
    case 4:
        if(b[1].getX() < b[2].getX() && b[1].getY() == b[2].getY())
        {
            b[0].setY(b[2].getY() + 1);
            b[1].setY(b[2].getY() + 1);
            b[3].setY(b[2].getY());
            b[0].setX(b[2].getX() - 1);
            b[1].setX(b[2].getX());
            b[3].setX(b[2].getX() + 1);
        }
        else if (b[1].getX() > b[2].getX() && b[1].getY() == b[2].getY()) {
            b[0].setY(b[2].getY() - 1);
            b[1].setY(b[2].getY() - 1);
            b[3].setY(b[2].getY());
            b[0].setX(b[2].getX() + 1);
            b[1].setX(b[2].getX());
            b[3].setX(b[2].getX() - 1);
        }
        else if (b[1].getX() == b[2].getX() && b[1].getY() > b[2].getY()) {
            b[0].setY(b[2].getY() + 1);
            b[1].setY(b[2].getY());
            b[3].setY(b[2].getY() - 1);
            b[0].setX(b[2].getX() + 1);
            b[1].setX(b[2].getX() + 1);
            b[3].setX(b[2].getX());
        }
        else if (b[1].getX() == b[2].getX() && b[1].getY() < b[2].getY()) {
            b[0].setY(b[2].getY() - 1);
            b[1].setY(b[2].getY());
            b[3].setY(b[2].getY() + 1);
            b[0].setX(b[2].getX() - 1);
            b[1].setX(b[2].getX() - 1);
            b[3].setX(b[2].getX());
        }
        break;
    case 5:
        if(b[0].getX() < b[3].getX() && b[0].getY() == b[3].getY())
        {
            b[0].setY(b[3].getY() + 1);
            b[1].setY(b[3].getY() + 1);
            b[2].setY(b[3].getY());
            b[0].setX(b[3].getX());
            b[1].setX(b[3].getX() + 1);
            b[2].setX(b[3].getX() - 1);
        }
        else if (b[0].getX() > b[3].getX() && b[0].getY() == b[3].getY()) {
            b[0].setY(b[3].getY() - 1);
            b[1].setY(b[3].getY() - 1);
            b[2].setY(b[3].getY());
            b[0].setX(b[3].getX());
            b[1].setX(b[3].getX() - 1);
            b[2].setX(b[3].getX() + 1);
        }
        else if (b[0].getX() == b[3].getX() && b[0].getY() > b[3].getY()) {
            b[0].setY(b[3].getY());
            b[1].setY(b[3].getY() - 1);
            b[2].setY(b[3].getY() + 1);
            b[0].setX(b[3].getX() + 1);
            b[1].setX(b[3].getX() + 1);
            b[2].setX(b[3].getX());
        }
        else if (b[0].getX() == b[3].getX() && b[0].getY() < b[3].getY()) {
            b[0].setY(b[3].getY());
            b[1].setY(b[3].getY() + 1);
            b[2].setY(b[3].getY() - 1);
            b[0].setX(b[3].getX() - 1);
            b[1].setX(b[3].getX() - 1);
            b[2].setX(b[3].getX());
        }
        break;
    case 6:
        if(b[0].getX() < b[2].getX() && b[0].getY() == b[2].getY())
        {
            b[0].setY(b[2].getY() + 1);
            b[1].setY(b[2].getY());
            b[3].setY(b[2].getY());
            b[0].setX(b[2].getX());
            b[1].setX(b[2].getX() - 1);
            b[3].setX(b[2].getX() + 1);
        }
        else if (b[0].getX() > b[2].getX() && b[0].getY() == b[2].getY()) {
            b[0].setY(b[2].getY() - 1);
            b[1].setY(b[2].getY());
            b[3].setY(b[2].getY());
            b[0].setX(b[2].getX());
            b[1].setX(b[2].getX() + 1);
            b[3].setX(b[2].getX() - 1);
        }
        else if (b[0].getX() == b[2].getX() && b[0].getY() > b[2].getY()) {
            b[0].setY(b[2].getY());
            b[1].setY(b[2].getY() + 1);
            b[3].setY(b[2].getY() - 1);
            b[0].setX(b[2].getX() + 1);
            b[1].setX(b[2].getX());
            b[3].setX(b[2].getX());
        }
        else if (b[0].getX() == b[2].getX() && b[0].getY() < b[2].getY()) {
            b[0].setY(b[2].getY());
            b[1].setY(b[2].getY() - 1);
            b[3].setY(b[2].getY() + 1);
            b[0].setX(b[2].getX() - 1);
            b[1].setX(b[2].getX());
            b[3].setX(b[2].getX());
        }
        break;
    default:
        break;
    }
    for (int i = 0; i < COUNT; i++)
        blocks[b[i].getX()][b[i].getY()] = color;
    return;
}


bool Item::is_legal(int op_type)
{
    switch (op_type)
    {
    case Operation_type::rotate:
    {
        break;
    }
    case Operation_type::moveLeft:
    {
        for(int i = 0; i < 4; i++)
        {
            if(b[i].getY() - 1 < 0)return false;
        }
        break;
    }
    case Operation_type::moveRight:
    {
        for(int i = 0; i < 4; i++)
        {
            if(b[i].getY() + 1 > 19)return false;
        }
        break;
    }
    default:
        break;
    }
    return true;
}
/*
    这一部分实现类Tetris中的函数
*/
bool Tetris::moveLeft()
{
    if (cube.is_legal(Operation_type::moveLeft) && is_legal(Operation_type::moveLeft))
    {
        cube.moveLeft(blocks);
        return true;
    }
    else
    {
        return false;
    }
}

bool Tetris::moveRight()
{
    if (cube.is_legal(Operation_type::moveRight) && is_legal(Operation_type::moveRight))
    {
        cube.moveRight(blocks);
        return true;
    }
    else
    {
        return false;
    }
}

bool Tetris::rotate()
{
    if (cube.is_legal(Operation_type::rotate) && is_legal(Operation_type::rotate))
    {
        cube.rotate(blocks);
        return true;
    }
    else
    {
        return false;
    }
}

bool Tetris::isOver()
{
    elimination();
    int *current = getBlocks(0);
    for(int i = 0; i < 10; i++)
    {
        if(current[i] >= 1)
            return true;
    }
    return false;
}

bool Tetris::haveFullLine()
{
    int *current;
    bool isFind = false;
    for(int i = 0; i < 20; i++)
    {
        bool isContinue = false;
        current = getBlocks(i);
        for(int j = 0; j < 10; j++)
        {
            if(current[j] == 0)
            {
                isContinue = true;
                break;
            }
        }
        if(isContinue)
            continue;
        isFind = true;
    }
    return isFind;
}

void Tetris::elimination()
{
    int count = 0;
    for(int i = 0; i < 20; i++)
    {
        bool isContinue = false;
        int *current = getBlocks(i);
        for(int j = 0; j < 10; j++)
        {
            if(current[j] == 0)
            {
                isContinue = true;
                break;
            }
        }
        if(isContinue)
            continue;
        for(int m = i; m > 0; m--)
        {
            for(int n = 0; n < 10; n++)
            {
                blocks[m][n] = blocks[m - 1][n];
            }
        }
        for(int n = 0; n < 10; n++)
        {
            blocks[0][n] = 0;
        }
        count++;
    }
    score += count * 100;
}

bool Tetris::is_legal(int op_type)
{
    switch (op_type)
    {
    case Operation_type::rotate:
    {
        bool isok = true;
        Item check = Item::newItem();
        for(int i = 0; i < COUNT; i++)
            blocks[cube.getX_(i)][cube.getY_(i)] = 0;
        switch (cube.getType())
        {
        case 0:
            break;
        case 1:
            if(cube.getX_(0) == cube.getX_(3))
            {
                check.b[0].setY(cube.getY_(1));
                check.b[1].setY(cube.getY_(1));
                check.b[2].setY(cube.getY_(1));
                check.b[3].setY(cube.getY_(1));
                check.b[0].setX(cube.getX_(1) - 1);
                check.b[1].setX(cube.getX_(1));
                check.b[2].setX(cube.getX_(1) + 1);
                check.b[3].setX(cube.getX_(1) + 2);
            }
            else {
                check.b[0].setY(cube.getY_(1) - 1);
                check.b[1].setY(cube.getY_(1));
                check.b[2].setY(cube.getY_(1) + 1);
                check.b[3].setY(cube.getY_(1) + 2);
                check.b[0].setX(cube.getX_(1));
                check.b[1].setX(cube.getX_(1));
                check.b[2].setX(cube.getX_(1));
                check.b[3].setX(cube.getX_(1));
            }
            break;
        case 2:
            if(cube.getX_(0) < cube.getX_(1) && cube.getY_(0) == cube.getY_(1))
            {
                check.b[0].setY(cube.getY_(2) + 1);
                check.b[1].setY(cube.getY_(2));
                check.b[2].setY(cube.getY_(2));
                check.b[3].setY(cube.getY_(2));
                check.b[0].setX(cube.getX_(2) - 1);
                check.b[1].setX(cube.getX_(2) - 1);
                check.b[2].setX(cube.getX_(2));
                check.b[3].setX(cube.getX_(2) + 1);
            }
            else if (cube.getX_(0) > cube.getX_(1) && cube.getY_(0) == cube.getY_(1)) {
                check.b[0].setY(cube.getY_(2) - 1);
                check.b[1].setY(cube.getY_(2));
                check.b[2].setY(cube.getY_(2));
                check.b[3].setY(cube.getY_(2));
                check.b[0].setX(cube.getX_(2) + 1);
                check.b[1].setX(cube.getX_(2) + 1);
                check.b[2].setX(cube.getX_(2));
                check.b[3].setX(cube.getX_(2) - 1);
            }
            else if (cube.getX_(0) == cube.getX_(1) && cube.getY_(0) > cube.getY_(1)) {
                check.b[0].setY(cube.getY_(2) + 1);
                check.b[1].setY(cube.getY_(2) + 1);
                check.b[2].setY(cube.getY_(2));
                check.b[3].setY(cube.getY_(2) - 1);
                check.b[0].setX(cube.getX_(2) + 1);
                check.b[1].setX(cube.getX_(2));
                check.b[2].setX(cube.getX_(2));
                check.b[3].setX(cube.getX_(2));
            }
            else if (cube.getX_(0) == cube.getX_(1) && cube.getY_(0) < cube.getY_(1)) {
                check.b[0].setY(cube.getY_(2) - 1);
                check.b[1].setY(cube.getY_(2) - 1);
                check.b[2].setY(cube.getY_(2));
                check.b[3].setY(cube.getY_(2) + 1);
                check.b[0].setX(cube.getX_(2) - 1);
                check.b[1].setX(cube.getX_(2));
                check.b[2].setX(cube.getX_(2));
                check.b[3].setX(cube.getX_(2));
            }
            break;
        case 3:
            if(cube.getX_(0) < cube.getX_(3) && cube.getY_(0) == cube.getY_(3))
            {
                check.b[0].setY(cube.getY_(2) + 1);
                check.b[1].setY(cube.getY_(2));
                check.b[2].setY(cube.getY_(2));
                check.b[3].setY(cube.getY_(2));
                check.b[0].setX(cube.getX_(2) + 1);
                check.b[1].setX(cube.getX_(2) - 1);
                check.b[2].setX(cube.getX_(2));
                check.b[3].setX(cube.getX_(2) + 1);
            }
            else if (cube.getX_(0) > cube.getX_(3) && cube.getY_(0) == cube.getY_(3)) {
                check.b[0].setY(cube.getY_(2) - 1);
                check.b[1].setY(cube.getY_(2));
                check.b[2].setY(cube.getY_(2));
                check.b[3].setY(cube.getY_(2));
                check.b[0].setX(cube.getX_(2) - 1);
                check.b[1].setX(cube.getX_(2) + 1);
                check.b[2].setX(cube.getX_(2));
                check.b[3].setX(cube.getX_(2) - 1);
            }
            else if (cube.getX_(0) == cube.getX_(3) && cube.getY_(0) > cube.getY_(3)) {
                check.b[0].setY(cube.getY_(2) - 1);
                check.b[1].setY(cube.getY_(2) + 1);
                check.b[2].setY(cube.getY_(2));
                check.b[3].setY(cube.getY_(2) - 1);
                check.b[0].setX(cube.getX_(2) + 1);
                check.b[1].setX(cube.getX_(2));
                check.b[2].setX(cube.getX_(2));
                check.b[3].setX(cube.getX_(2));
            }
            else if (cube.getX_(0) == cube.getX_(3) && cube.getY_(0) < cube.getY_(3)) {
                check.b[0].setY(cube.getY_(2) + 1);
                check.b[1].setY(cube.getY_(2) - 1);
                check.b[2].setY(cube.getY_(2));
                check.b[3].setY(cube.getY_(2) + 1);
                check.b[0].setX(cube.getX_(2) - 1);
                check.b[1].setX(cube.getX_(2));
                check.b[2].setX(cube.getX_(2));
                check.b[3].setX(cube.getX_(2));
            }
            break;
        case 4:
            if(cube.getX_(1) < cube.getX_(2) && cube.getY_(1) == cube.getY_(2))
            {
                check.b[0].setY(cube.getY_(2) + 1);
                check.b[1].setY(cube.getY_(2) + 1);
                check.b[2].setY(cube.getY_(2));
                check.b[3].setY(cube.getY_(2));
                check.b[0].setX(cube.getX_(2) - 1);
                check.b[1].setX(cube.getX_(2));
                check.b[2].setX(cube.getX_(2));
                check.b[3].setX(cube.getX_(2) + 1);
            }
            else if (cube.getX_(1) > cube.getX_(2) && cube.getY_(1) == cube.getY_(2)) {
                check.b[0].setY(cube.getY_(2) - 1);
                check.b[1].setY(cube.getY_(2) - 1);
                check.b[2].setY(cube.getY_(2));
                check.b[3].setY(cube.getY_(2));
                check.b[0].setX(cube.getX_(2) + 1);
                check.b[1].setX(cube.getX_(2));
                check.b[2].setX(cube.getX_(2));
                check.b[3].setX(cube.getX_(2) - 1);
            }
            else if (cube.getX_(1) == cube.getX_(2) && cube.getY_(1) > cube.getY_(2)) {
                check.b[0].setY(cube.getY_(2) + 1);
                check.b[1].setY(cube.getY_(2));
                check.b[2].setY(cube.getY_(2));
                check.b[3].setY(cube.getY_(2) - 1);
                check.b[0].setX(cube.getX_(2) + 1);
                check.b[1].setX(cube.getX_(2) + 1);
                check.b[2].setX(cube.getX_(2));
                check.b[3].setX(cube.getX_(2));
            }
            else if (cube.getX_(1) == cube.getX_(2) && cube.getY_(1) < cube.getY_(2)) {
                check.b[0].setY(cube.getY_(2) - 1);
                check.b[1].setY(cube.getY_(2));
                check.b[2].setY(cube.getY_(2));
                check.b[3].setY(cube.getY_(2) + 1);
                check.b[0].setX(cube.getX_(2) - 1);
                check.b[1].setX(cube.getX_(2) - 1);
                check.b[2].setX(cube.getX_(2));
                check.b[3].setX(cube.getX_(2));
            }
            break;
        case 5:
            if(cube.getX_(0) < cube.getX_(3) && cube.getY_(0) == cube.getY_(3))
            {
                check.b[0].setY(cube.getY_(3) + 1);
                check.b[1].setY(cube.getY_(3) + 1);
                check.b[2].setY(cube.getY_(3));
                check.b[3].setY(cube.getY_(3));
                check.b[0].setX(cube.getX_(3));
                check.b[1].setX(cube.getX_(3) + 1);
                check.b[2].setX(cube.getX_(3) - 1);
                check.b[3].setX(cube.getX_(3));
            }
            else if (cube.getX_(0) > cube.getX_(3) && cube.getY_(0) == cube.getY_(3)) {
                check.b[0].setY(cube.getY_(3) - 1);
                check.b[1].setY(cube.getY_(3) - 1);
                check.b[2].setY(cube.getY_(3));
                check.b[3].setY(cube.getY_(3));
                check.b[0].setX(cube.getX_(3));
                check.b[1].setX(cube.getX_(3) - 1);
                check.b[2].setX(cube.getX_(3) + 1);
                check.b[3].setX(cube.getX_(3));
            }
            else if (cube.getX_(0) == cube.getX_(3) && cube.getY_(0) > cube.getY_(3)) {
                check.b[0].setY(cube.getY_(3));
                check.b[1].setY(cube.getY_(3) - 1);
                check.b[2].setY(cube.getY_(3) + 1);
                check.b[3].setY(cube.getY_(3));
                check.b[0].setX(cube.getX_(3) + 1);
                check.b[1].setX(cube.getX_(3) + 1);
                check.b[2].setX(cube.getX_(3));
                check.b[3].setX(cube.getX_(3));
            }
            else if (cube.getX_(0) == cube.getX_(3) && cube.getY_(0) < cube.getY_(3)) {
                check.b[0].setY(cube.getY_(3));
                check.b[1].setY(cube.getY_(3) + 1);
                check.b[2].setY(cube.getY_(3) - 1);
                check.b[3].setY(cube.getY_(3));
                check.b[0].setX(cube.getX_(3) - 1);
                check.b[1].setX(cube.getX_(3) - 1);
                check.b[2].setX(cube.getX_(3));
                check.b[3].setX(cube.getX_(3));
            }
            break;
        case 6:
            if(cube.getX_(0) < cube.getX_(2) && cube.getY_(0) == cube.getY_(2))
            {
                check.b[0].setY(cube.getY_(2) + 1);
                check.b[1].setY(cube.getY_(2));
                check.b[2].setY(cube.getY_(2));
                check.b[3].setY(cube.getY_(2));
                check.b[0].setX(cube.getX_(2));
                check.b[1].setX(cube.getX_(2) - 1);
                check.b[2].setX(cube.getX_(2));
                check.b[3].setX(cube.getX_(2) + 1);
            }
            else if (cube.getX_(0) > cube.getX_(2) && cube.getY_(0) == cube.getY_(2)) {
                check.b[0].setY(cube.getY_(2) - 1);
                check.b[1].setY(cube.getY_(2));
                check.b[2].setY(cube.getY_(2));
                check.b[3].setY(cube.getY_(2));
                check.b[0].setX(cube.getX_(2));
                check.b[1].setX(cube.getX_(2) + 1);
                check.b[2].setX(cube.getX_(2));
                check.b[3].setX(cube.getX_(2) - 1);
            }
            else if (cube.getX_(0) == cube.getX_(2) && cube.getY_(0) > cube.getY_(2)) {
                check.b[0].setY(cube.getY_(2));
                check.b[1].setY(cube.getY_(2) + 1);
                check.b[2].setY(cube.getY_(2));
                check.b[3].setY(cube.getY_(2) - 1);
                check.b[0].setX(cube.getX_(2) + 1);
                check.b[1].setX(cube.getX_(2));
                check.b[2].setX(cube.getX_(2));
                check.b[3].setX(cube.getX_(2));
            }
            else if (cube.getX_(0) == cube.getX_(2) && cube.getY_(0) < cube.getY_(2)) {
                check.b[0].setY(cube.getY_(2));
                check.b[1].setY(cube.getY_(2) - 1);
                check.b[2].setY(cube.getY_(2));
                check.b[3].setY(cube.getY_(2) + 1);
                check.b[0].setX(cube.getX_(2) - 1);
                check.b[1].setX(cube.getX_(2));
                check.b[2].setX(cube.getX_(2));
                check.b[3].setX(cube.getX_(2));
            }
            break;
        default:
            break;
        }
        for(int i = 0; i < COUNT; i++)
        {
            if(check.getX_(i) < 0
                || check.getX_(i) > 19
                || check.getY_(i) < 0
                || check.getY_(i) > 9)
                isok = false;
            if(blocks[check.getX_(i)][check.getY_(i)] >= 1)
                isok = false;
        }
        for(int i = 0; i < COUNT; i++)//判断两点：有没有越界，本身有没有东西
            blocks[cube.getX_(i)][cube.getY_(i)] = cube.getColor();
        if(!isok)
            return isok;
        break;
    }
    case Operation_type::moveLeft:
    {
        bool isok = true;
        for(int i = 0; i < COUNT; i++)
            blocks[cube.getX_(i)][cube.getY_(i)] = 0;
        for(int i = 0; i < COUNT; i++)
        {
            if(cube.getY_(i) - 1 < 0||blocks[cube.getX_(i)][cube.getY_(i) - 1] >= 1)
                isok = false;
        }
        for(int i = 0; i < COUNT; i++)
            blocks[cube.getX_(i)][cube.getY_(i)] = cube.getColor();
        if(!isok)
            return isok;
        break;
    }
    case Operation_type::moveRight:
    {
        bool isok = true;
        for(int i = 0; i < COUNT; i++)
            blocks[cube.getX_(i)][cube.getY_(i)] = 0;
        for(int i = 0; i < COUNT; i++)
        {
            if(cube.getY_(i) + 1 >= MAXY||blocks[cube.getX_(i)][cube.getY_(i) + 1] >= 1)
                isok = false;
        }
        for(int i = 0; i < COUNT; i++)
            blocks[cube.getX_(i)][cube.getY_(i)] = cube.getColor();
        if(!isok)
            return isok;
        break;
    }
    default:
        break;
    }
    return true;
}

bool Tetris::isBottom()
{
    bool isbottom = false;
    for(int i = 0; i < COUNT; i++)
        blocks[cube.getX_(i)][cube.getY_(i)] = 0;
    for(int i = 0; i < COUNT; i++)
    {
        if(blocks[cube.getX_(i) + 1][cube.getY_(i)] >= 1 || cube.getX_(i) == 19)
        {
            isbottom = true;
        }
    }
    for(int i = 0; i < COUNT; i++)
        blocks[cube.getX_(i)][cube.getY_(i)] = cube.getColor();
    if(isbottom)
        elimination();
    return isbottom;
}

void Tetris::updateItem()
{
    cube = nextCube;
    return;
}

void Tetris::updateNextItem()
{
    Item next_cube = Item::newItem();
    nextCube = next_cube;
    return;
}

void Tetris::clear()
{
    for (int i = 0; i < 20; i++) {
        for(int j = 0; j < 10; j++)
            blocks[i][j] = 0;
    }
    score = 0;
}

