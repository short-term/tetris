#include "operation.h"

void Operation::setDefault()
{
    m_left = Qt::Key_A;
    m_right = Qt::Key_D;
    r_left = Qt::Key_J;
    r_right = Qt::Key_L;
    down = Qt::Key_S;
}

int Operation::getKeyCode(QChar k){
    QKeySequence keySequence(k);
    return keySequence[0];
}

void Operation::setGetDown(QChar k){
    int keyCode = getKeyCode(k);
    if(keyCode < 'A' || keyCode > 'Z') return ;
    down = (Qt::Key)keyCode;
}

void Operation::setMoveLeft(QChar k){
    int keyCode = getKeyCode(k);
    if(keyCode < 'A' || keyCode > 'Z') return ;
    m_left = (Qt::Key)keyCode;
}

void Operation::setMoveRight(QChar k){
    int keyCode = getKeyCode(k);
    if(keyCode < 'A' || keyCode > 'Z') return ;
    m_right = (Qt::Key)keyCode;
}

void Operation::setRotateLeft(QChar k){
    int keyCode = getKeyCode(k);
    if(keyCode < 'A' || keyCode > 'Z') return ;
    r_left = (Qt::Key)keyCode;
}

void Operation::setRotateRight(QChar k){
    int keyCode = getKeyCode(k);
    if(keyCode < 'A' || keyCode > 'Z') return ;
    r_right = (Qt::Key)keyCode;
}
