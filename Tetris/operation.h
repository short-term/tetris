#ifndef OPERATION_H
#define OPERATION_H

#include <QKeyEvent>
#include <QChar>
#include <QString>

class Operation{
public:
    Operation(){
        m_left = Qt::Key_A;
        m_right = Qt::Key_D;
        r_left = Qt::Key_J;
        r_right = Qt::Key_L;
        down = Qt::Key_S;//默认按键
    }

    int getKeyCode(QChar k);

    void setMoveLeft(QChar k);
    void setMoveRight(QChar k);
    void setRotateLeft(QChar k);
    void setRotateRight(QChar k);
    void setGetDown(QChar k);
    void setDefault(); //重置为默认

    Qt::Key getMoveLeft() {
        return  m_left;
    }

    Qt::Key getMoveRight() {
        return  m_right;
    }

    Qt::Key getRotateLeft() {
        return  r_left;
    }

    Qt::Key getRotateRight() {
        return  r_right;
    }

    Qt::Key getDown() {
        return  down;
    }

public:
    Qt::Key m_left;
    Qt::Key m_right;
    Qt::Key r_left;
    Qt::Key r_right;
    Qt::Key down;

};



#endif // OPERATION_H
