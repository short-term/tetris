#include "nexttetris.h"

Nexttetris::Nexttetris(QWidget *parent):QWidget(parent)
{
    // 初始化 to be completed
    for (int i = 0; i < COUNT; i++) {
        nextCube.b[i].setX(-1);
        nextCube.b[i].setY(-1);
    }
    int w = Tetris::getNextWidth();
    int h = Tetris::getNextHeight();
    setFixedSize(w, h);
    QPalette palette;
    palette.setBrush(QPalette::Background, QBrush(QPixmap(":/img/background3.png")));
    this->setPalette(palette);
    //setPalette(QPalette(Qt::black));

    setAutoFillBackground(true);
}


void Nexttetris::updateTetris(Tetris tetris) {
    nextCube = tetris.getNextCube();

    for (int i = 0; i < COUNT; i++) {
        int x = nextCube.b[i].getX();
        x += RESTX / 2;
        nextCube.b[i].setX(x);

        int y = nextCube.b[i].getY();
        y -= nextCube.getStart();
        y += RESTY + 1;
        if(nextCube.getType() == 1)
            y -= 2;
        nextCube.b[i].setY(y);
    }

    repaint();
}


void Nexttetris::paintEvent(QPaintEvent *event) {
    QPainter painter(this);





    for (int i = 0; i < COUNT; i++) {
        int color = nextCube.getColor();
        int x = nextCube.b[i].getX();
        int y = nextCube.b[i].getY();
        int x1 = y * WIDTH + y * INTERVAL;
        int y1 = x * HEIGHT + x * INTERVAL;
        //painter.drawRect(x1, y1, WIDTH, HEIGHT);
        switch(color) {
        case 1: painter.drawPixmap(x1, y1, WIDTH, HEIGHT, QPixmap(":/img/blue.jpg")); break;
        case 2: painter.drawPixmap(x1, y1, WIDTH, HEIGHT, QPixmap(":/img/red.jpg")); break;
        case 3: painter.drawPixmap(x1, y1, WIDTH, HEIGHT, QPixmap(":/img/yellow.png")); break;
        case 4: painter.drawPixmap(x1, y1, WIDTH, HEIGHT, QPixmap(":/img/green.jpg")); break;
        case 5: painter.drawPixmap(x1, y1, WIDTH, HEIGHT, QPixmap(":/img/grey.jpg")); break;
        default: break;
        }

    }
}
