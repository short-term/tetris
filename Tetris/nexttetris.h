#ifndef NEXTTETRIS_H
#define NEXTTETRIS_H

#include <QWidget>
#include <QPen>
#include <QBrush>
#include <QColor>
#include <QPaintEvent>
#include <QPainter>
#include <QPalette>

#include <QLabel>
#include <QPixmap>
#include <QDebug>

#include "tetris.h"

#define RESTY (MAXX - NEXX) / 2     //方块x坐标的转换常数
#define RESTX 4                         //方块y坐标的转换常数

class Nexttetris: public QWidget
{
    Q_OBJECT

public:
    Nexttetris(QWidget *parent = nullptr);
    void updateTetris(Tetris tetris);
    void paintEvent(QPaintEvent *event);

signals:

public slots:

private:
    Item nextCube;
};

#endif // NEXTTETRIS_H
